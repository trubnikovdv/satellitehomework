using System;
using NUnit.Framework;
using SatelliteInterface.computation.utils;

namespace SatelliteInterfaceTest.computation.utils
{
    public class EccentricAnomalyTest
    {
        [Test]
        public void TestCalculateEccentricAnomaly()
        {
            const double precision = 1e-12;
            
            var random = new Random((int) (DateTime.Now.Ticks % int.MaxValue));
            var eccentricity = random.NextDouble(0.009, 0.010);
            var eccentricAnomaly = random.NextDouble(0, 2 * Math.PI);

            // Kepler equation for elliptical motion
            var meanAnomaly = eccentricAnomaly - eccentricity * Math.Sin(eccentricAnomaly);
            
            var calculateEccentricAnomaly = KeplerUtils.CalculateEccentricAnomaly(meanAnomaly, eccentricity, precision / 100);
            
            Assert.AreEqual(eccentricAnomaly, calculateEccentricAnomaly, precision);
        }
    }

    internal static class RandomExtensions
    {
        public static double NextDouble(this Random random, double from, double to)
        {
            if (to <= from) throw new ArgumentException($"[{to}] less than [{from}]");

            var range = to - from;
            var sample = random.NextDouble();
            var scaled = (sample * range) + from;
            return scaled;
        }
    }
}