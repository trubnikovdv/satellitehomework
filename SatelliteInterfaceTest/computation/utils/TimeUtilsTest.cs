using System;
using NUnit.Framework;
using SatelliteInterface.computation.utils;

namespace SatelliteInterfaceTest.computation.utils
{
    public class TimeOfWeekTests
    {
        private const double DELTA = 0.00001;
        
        [Test]
        public void TestTimeOfWeekSunday()
        {
            const int timeOfWeekExpected = 12 * 60 * 60 + 6 * 60 + 30;
            
            var time = new DateTime(2019, 10, 27, 12, 6, 30, DateTimeKind.Utc);
            var timeOfWeek = time.TimeOfWeek();

            Assert.AreEqual(timeOfWeekExpected, timeOfWeek, DELTA);
        }
        
        [Test]
        public void TestTimeOfWeekThursday()
        {
            const int timeOfWeekExpected = 4 * 24 * 60 * 60 + 42;
            
            var time = new DateTime(2019, 10, 31, 00, 00, 42, DateTimeKind.Utc);
            var timeOfWeek = time.TimeOfWeek();

            Assert.AreEqual(timeOfWeekExpected, timeOfWeek, DELTA);
        }
        
        [Test]
        public void TestTimeOfWeekSaturday()
        {
            const int timeOfWeekExpected = 7 * 24 * 60 * 60 - 1;
            
            var time = new DateTime(2021, 01, 30, 23, 59, 59, DateTimeKind.Utc);
            var timeOfWeek = time.TimeOfWeek();

            Assert.AreEqual(timeOfWeekExpected, timeOfWeek, DELTA);
        }
        
        [Test]
        public void TestTimeOfWeekHomework()
        {
            const int timeOfWeekExpected = 270000;
            
            var time = new DateTime(2019, 09, 18, 03, 00, 00, DateTimeKind.Utc);
            var timeOfWeek = time.TimeOfWeek();

            Assert.AreEqual(timeOfWeekExpected, timeOfWeek, DELTA);
        }
    }
    
    public class LeapSecondsTests
    {
        [Test]
        public void BeforeGps()
        {
            var beforeGps = new DateTime(1980, 1, 5, 00, 00, 00, DateTimeKind.Utc);
            Assert.That(() => TimeUtils.CountLeapSeconds(beforeGps), 
                Throws.TypeOf<ArgumentOutOfRangeException>());
        }

        [Test]
        public void Year1988()
        {
            var year2019 = new DateTime(1988, 01, 01, 00, 00, 00, DateTimeKind.Utc);
            Assert.AreEqual(5, TimeUtils.CountLeapSeconds(year2019));
        }

        [Test]
        public void Year2000()
        {
            var year2019 = new DateTime(2000, 01, 01, 00, 00, 00, DateTimeKind.Utc);
            Assert.AreEqual(13, TimeUtils.CountLeapSeconds(year2019));
        }

        [Test]
        public void Year2019()
        {
            var year2019 = new DateTime(2019, 10, 26, 22, 51, 37, DateTimeKind.Utc);
            Assert.AreEqual(18, TimeUtils.CountLeapSeconds(year2019));
        }
    }
}