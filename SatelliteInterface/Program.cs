﻿using System;
using System.Collections.Generic;
using SatelliteInterface.computation.coordinates;
using SatelliteInterface.computation.datasources;
using SatelliteInterface.computation.datasources.almanac;
using SatelliteInterface.computation.utils;

namespace SatelliteInterface
{
    internal static class Program
    {
        private static readonly Dictionary<int, IDataSource> AlmanacMap = new Dictionary<int, IDataSource>(10);
        public static void Main()
        {
            InitAlmanacs();

            foreach (int prn in AlmanacMap.Keys)
            {
                CalculateSatellite(prn);
                Console.WriteLine("--------------------------");
            }
        }

        private static void CalculateSatellite(int prn)
        {
            var bmstuCoordinates = new GeocentricCoordinates(x: 2846228.896, y: 2198658.103, z: 5249983.343);
            var almanac = AlmanacMap.GetValueOrDefault(prn);

            var currentTime = DateTime.Now;
            Console.WriteLine($"From week: {currentTime.TimeOfWeek()}");
            Console.WriteLine($"PRN={prn}, [{currentTime.ToLongDateString()} {currentTime.ToLongTimeString()}]");

            var geocentric = almanac.GetPositionForTime(currentTime);
            var azEl = bmstuCoordinates.ApparentPosition(geocentric);

            Console.WriteLine($"{azEl}");
        }

        #region hardcode_alamanac
        private static void InitAlmanacs()
        {
            AlmanacMap.Add(01, new AlmanacGpsYuma(
                eccentricity: 0.9131431580E-002,
                timeOfApplicability: 147456.0000,
                orbitalInclination: 0.9777833226,
                rightAscensionRate: -0.7886042771E-008,
                sqrtSemiMajorAxis: 5153.596191,
                rightAscension: 0.4236753070E+000,
                perigeeArgument: 0.766247747,
                meanAnomaly: -0.9547139144E+000,
                zeroOrderClockCorrection: -0.1802444458E-003,
                firstOrderClockCorrection: -0.1455191523E-010,
                week: 30
            ));
            AlmanacMap.Add(03, new AlmanacGpsYuma(
                eccentricity: 0.2588748932E-002,
                timeOfApplicability: 147456.0000,
                orbitalInclination: 0.9636599136,
                rightAscensionRate: -0.7806039439E-008,
                sqrtSemiMajorAxis: 5153.629883,
                rightAscension: 0.1465371475E+001,
                perigeeArgument: 0.788896808,
                meanAnomaly: -0.2124580993E+001,
                zeroOrderClockCorrection: -0.1802444458E-003,
                firstOrderClockCorrection: -0.1455191523E-010,
                week: 30
            ));
            AlmanacMap.Add(05, new AlmanacGpsYuma(
                eccentricity: 0.5832195282E-002,
                timeOfApplicability: 147456.0000,
                orbitalInclination: 0.9505491716,
                rightAscensionRate: -0.7943188009E-008,
                sqrtSemiMajorAxis: 5153.489258,
                rightAscension: 0.1436296996E+001,
                perigeeArgument: 0.800385935,
                meanAnomaly: 0.1884657185E+001,
                zeroOrderClockCorrection: -0.1802444458E-003,
                firstOrderClockCorrection: -0.1455191523E-010,
                week: 30
            ));
            AlmanacMap.Add(07, new AlmanacGpsYuma(
                eccentricity: 0.1317119598E-001,
                timeOfApplicability: 147456.0000,
                orbitalInclination: 0.9546957134,
                rightAscensionRate: -0.7977475151E-008,
                sqrtSemiMajorAxis: 5153.666504,
                rightAscension: -0.2711176969E+001,
                perigeeArgument: -2.423481044,
                meanAnomaly: -0.1624783013E+001,
                zeroOrderClockCorrection: -0.1802444458E-003,
                firstOrderClockCorrection: -0.1455191523E-010,
                week: 30
            ));
            AlmanacMap.Add(09, new AlmanacGpsYuma(
                eccentricity: 0.1598358154E-002,
                timeOfApplicability: 147456.0000,
                orbitalInclination: 0.9526643872,
                rightAscensionRate: -0.7863184676E-008,
                sqrtSemiMajorAxis: 5153.513672,
                rightAscension: 0.2497242451E+001,
                perigeeArgument: 1.672635648,
                meanAnomaly: 0.1721373244E+001,
                zeroOrderClockCorrection: -0.1802444458E-003,
                firstOrderClockCorrection: -0.1455191523E-010,
                week: 30
            ));
            AlmanacMap.Add(16, new AlmanacGpsYuma(
                eccentricity: 0.1090812683E-001,
                timeOfApplicability: 147456.0000,
                orbitalInclination: 0.9809171975,
                rightAscensionRate: -0.7703178011E-008,
                sqrtSemiMajorAxis: 5153.618652,
                rightAscension: -0.1589089872E+001,
                perigeeArgument: 0.578414121,
                meanAnomaly: 0.2191619998E+001,
                zeroOrderClockCorrection: -0.1783370972E-003,
                firstOrderClockCorrection: -0.1455191523E-010,
                week: 30
            ));
            AlmanacMap.Add(17, new AlmanacGpsYuma(
                eccentricity: 0.1331281662E-001,
                timeOfApplicability: 147456.0000,
                orbitalInclination: 0.9843986148,
                rightAscensionRate: -0.7726036106E-008,
                sqrtSemiMajorAxis: 5153.552734,
                rightAscension: -0.5740784531E+000,
                perigeeArgument: -1.693917010,
                meanAnomaly: 0.1158545731E+001,
                zeroOrderClockCorrection: -0.1783370972E-003,
                firstOrderClockCorrection: -0.1455191523E-010,
                week: 30
            ));
            AlmanacMap.Add(24, new AlmanacGpsYuma(
                eccentricity: 0.8992671967E-002,
                timeOfApplicability: 147456.0000,
                orbitalInclination: 0.9379417670,
                rightAscensionRate: -0.8148910863E-008,
                sqrtSemiMajorAxis: 5153.610352,
                rightAscension: -0.2779837215E+001,
                perigeeArgument: 0.617677812,
                meanAnomaly: -0.6858219877E+000,
                zeroOrderClockCorrection: -0.1783370972E-003,
                firstOrderClockCorrection: -0.1455191523E-010,
                week: 30
            ));
            AlmanacMap.Add(29, new AlmanacGpsYuma(
                eccentricity: 0.1102924347E-002,
                timeOfApplicability: 147456.0000,
                orbitalInclination: 0.9859565640,
                rightAscensionRate: -0.7726036106E-008,
                sqrtSemiMajorAxis: 5153.629883,
                rightAscension: -0.5626773356E+000,
                perigeeArgument: 2.057104189,
                meanAnomaly: 0.1398566162E+001,
                zeroOrderClockCorrection: -0.1783370972E-003,
                firstOrderClockCorrection: -0.1455191523E-010,
                week: 30
            ));
            AlmanacMap.Add(30, new AlmanacGpsYuma(
                eccentricity: 0.4159450531E-002,
                timeOfApplicability: 147456.0000,
                orbitalInclination: 0.9400749590,
                rightAscensionRate: -0.8126052768E-008,
                sqrtSemiMajorAxis: 5153.673340,
                rightAscension: -0.2685422121E+001,
                perigeeArgument: -2.905422904,
                meanAnomaly: -0.1665514023E+001,
                zeroOrderClockCorrection: -0.1783370972E-003,
                firstOrderClockCorrection: -0.1455191523E-010,
                week: 30
            ));
        }
        #endregion
    }
}