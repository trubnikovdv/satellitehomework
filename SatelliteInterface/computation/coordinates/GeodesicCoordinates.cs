using SatelliteInterface.computation.utils;

namespace SatelliteInterface.computation.coordinates
{
    public struct GeodesicCoordinates
    {
        public readonly double Latitude;
        public readonly double Longitude;

        public GeodesicCoordinates(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public override string ToString()
        {
            return $"широта: {Latitude.ToDegree()}, долгота: {Longitude.ToDegree()}";
        }
    }
}