using System;

namespace SatelliteInterface.computation.coordinates
{
    public struct OrbitalCoordinates
    {
        public readonly double X;
        public readonly double Y;

        public OrbitalCoordinates(PolarCoordinates polar)
        {
            X = polar.Radius * Math.Cos(polar.Angle);
            Y = polar.Radius * Math.Sin(polar.Angle);
        }
    }
}