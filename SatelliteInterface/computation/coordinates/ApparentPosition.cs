using SatelliteInterface.computation.utils;

namespace SatelliteInterface.computation.coordinates
{
    public struct ApparentPosition
    {
        public readonly double Azimuth;
        public readonly double Elevation;

        public ApparentPosition(double azimuth, double elevation)
        {
            Azimuth = azimuth;
            Elevation = elevation;
        }

        public override string ToString()
        {
            return $"азимут: {Azimuth.ToDegree()}, угол места: {Elevation.ToDegree()}";
        }
    }
}