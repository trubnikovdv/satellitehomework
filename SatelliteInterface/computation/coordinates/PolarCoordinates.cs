using SatelliteInterface.computation.utils;

namespace SatelliteInterface.computation.coordinates
{
    public struct PolarCoordinates
    {
        public readonly double Angle;
        public readonly double Radius;

        public PolarCoordinates(double angle, double radius)
        {
            Angle = angle;
            Radius = radius;
        }

        public override string ToString()
        {
            return $"радиус: {Radius.Round(0)} [м], угол: {Angle.ToDegree()}";
        }
    }
}