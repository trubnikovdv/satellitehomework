using System;
using SatelliteInterface.computation.utils;

namespace SatelliteInterface.computation.coordinates
{
    public struct GeocentricCoordinates
    {
        public readonly double X;
        public readonly double Y;
        public readonly double Z;

        public GeocentricCoordinates(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public GeocentricCoordinates(OrbitalCoordinates orbital, double Ωk, double i)
        {
            X = orbital.X * Math.Cos(Ωk) - orbital.Y * Math.Cos(i) * Math.Sin(Ωk);
            Y = orbital.X * Math.Sin(Ωk) + orbital.Y * Math.Cos(i) * Math.Cos(Ωk);
            Z = orbital.Y * Math.Sin(i);
        }

        public override string ToString()
        {
            return $"x: {X.Round(0)} [м], y: {Y.Round(0)} [м], z: {Z.Round(0)} [м]";
        }
    }
}