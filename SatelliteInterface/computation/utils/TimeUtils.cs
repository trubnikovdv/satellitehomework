using System;
using System.Linq;

namespace SatelliteInterface.computation.utils
{

    public static class TimeUtils
    {
        public const int SECONDS_IN_MINUTE = 60;
        public const int SECONDS_IN_HOUR = SECONDS_IN_MINUTE * 60;
        public const int SECONDS_IN_DAY = SECONDS_IN_HOUR * 24;
        public const int SECONDS_IN_WEEK = SECONDS_IN_DAY * 7;

        public static bool Before(this DateTime left, DateTime right)
        {
            return left.CompareTo(right) == -1;
        }

        public static bool After(this DateTime left, DateTime right)
        {
            return left.CompareTo(right) == 1;
        }

        public static double TimeOfWeek(this DateTime dateTime)
        {
            var dayOfWeek = (int) dateTime.DayOfWeek;
            
            double timeOfDay = dateTime.TimeOfDay.TotalSeconds;
            
            return timeOfDay + dayOfWeek * SECONDS_IN_DAY;
        }

        public static int CountLeapSeconds(DateTime date)
        {
            var utcDate = date.ToUniversalTime();

            if (utcDate.Before(LeapSeconds.GpsStart))
            {
                throw new ArgumentOutOfRangeException($"Дата не может быть раньше, чем \"{LeapSeconds.GpsStart.ToLongDateString()}\"");
            }

            return LeapSeconds.LeapDateList
                .TakeWhile(leapDate => utcDate.After(leapDate))
                .Count();
        }
    }
}