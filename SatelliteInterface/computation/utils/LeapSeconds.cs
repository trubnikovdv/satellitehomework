using System;
using System.Collections.Generic;

namespace SatelliteInterface.computation.utils
{
    public static class LeapSeconds
    {
        public static readonly DateTime GpsStart = new DateTime(1980, 01, 06, 0, 0, 0);

        public static readonly List<DateTime> LeapDateList = new List<DateTime>
        {
            Jule30(1981),
            Jule30(1982),
            Jule30(1983),
            Jule30(1985),
            December31(1987),
            December31(1989),
            December31(1990),
            Jule30(1992),
            Jule30(1993),
            Jule30(1994),
            December31(1995),
            Jule30(1997),
            December31(1998),
            December31(2005),
            December31(2008),
            Jule30(2012),
            Jule30(2015),
            December31(2016)
        };

        private static DateTime Jule30(int year)
        {
            return new DateTime(year, 06, 30, 23, 59, 59);
        }
        
        private static DateTime December31(int year)
        {
            return new DateTime(year, 12, 31, 23, 59, 59);
        }
    }
}