using System;

namespace SatelliteInterface.computation.utils
{
    public static class KeplerUtils
    {
        public const double EARTH_GRAVI_CONSTANT = 3.986005e+14;
        public const double EARTH_ROTATION_RATE = 7.2921151467e-5;

        public static double CalculateEccentricAnomaly(double meanAnomaly, double eccentricity, double precision = 1e-10)
        {
            const int maxTtl = int.MaxValue / 1_000;
            var ttl = 0;
            double eccentricAnomaly = meanAnomaly;
            
            while(true)
            {
                double value = meanAnomaly + eccentricity * Math.Sin(eccentricAnomaly);
                if (Math.Abs(eccentricAnomaly - value) < precision)
                {
                    eccentricAnomaly = value;
                    break;
                }
                if (++ttl == maxTtl)
                {
                    throw new ArithmeticException($"Заданная точность [{precision}] недостижима.");
                }
                eccentricAnomaly = value;
            }

            return eccentricAnomaly.Normalize();
        }

        public static double CalculateTrueAnomaly(double eccentricAnomaly, double eccentricity)
        {
            double eSqrt = Math.Sqrt(1 - Math.Pow(eccentricity, 2));
            double sinE = Math.Sin(eccentricAnomaly);
            double cosE = Math.Cos(eccentricAnomaly);

            double trueAnomaly = Math.Atan2(eSqrt * sinE, cosE - eccentricity);
            return trueAnomaly.Normalize();
        }
    }
}