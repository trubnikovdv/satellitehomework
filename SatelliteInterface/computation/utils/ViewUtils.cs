using System;

namespace SatelliteInterface.computation.utils
{
    public static class ViewUtils
    {
        public static string ToDegree(this double rad)
        {
            double degree =  Math.Round(rad * 360 / 2 / Math.PI, 2);
            return $"{degree}°";
        }

        public static double Normalize(this double rad)
        {
            const double period = 2 * Math.PI;
            return (rad % period + period) % period;
        }

        public static string Round(this double value, int digits = 2)
        {
            double rounded =  Math.Round(value, digits);
            return $"{rounded}";
        }
    }
}