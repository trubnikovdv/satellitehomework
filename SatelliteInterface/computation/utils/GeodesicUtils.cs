using System;
using SatelliteInterface.computation.coordinates;

namespace SatelliteInterface.computation.utils
{
    using static Math;

    public static class GeodesicUtils
    {
        private const double EARTH_ECCENTRICITY = 0.01671123;
        private const double EARTH_SEMI_AXIS = 149.598261e+6;

        public static GeodesicCoordinates ToGeodesic(this GeocentricCoordinates geocentric, double precision = 1e-10)
        {
            double geodesicLongitude = Atan2(geocentric.Y, geocentric.X);
            
            double diagonalXY = Sqrt(Pow(geocentric.X, 2) + Pow(geocentric.Y, 2));
            double radius = Sqrt(Pow(diagonalXY, 2) + Pow(geocentric.Z, 2));
            double geocentricLatitude = Atan2(geocentric.Z, diagonalXY);

            double powEccentricity = Pow(EARTH_ECCENTRICITY, 2);
            double p = powEccentricity * EARTH_SEMI_AXIS / 2 / radius;

            double s1 = 2 * precision;
            double s2 = 0;
            double geodesicLatitude = 0;

            const int maxTtl = int.MaxValue / 1_000;
            var ttl = 0;

            while (Abs(s1 - s2) > precision)
            {
                s1 = s2;
                geodesicLatitude = geocentricLatitude + s1;
                s2 = Asin(p * Sin(2 * geodesicLatitude) / Sqrt(1 - powEccentricity * Sin(geodesicLatitude)));

                if (++ttl > maxTtl)
                {
                    throw new ArithmeticException($"Заданная точность [{precision}] недостижима.");
                }
            }

            return new GeodesicCoordinates(latitude: geodesicLatitude, longitude: geodesicLongitude);
        }

        public static ApparentPosition ApparentPosition(this GeocentricCoordinates antenna, GeocentricCoordinates satellite)
        {
            double dX = satellite.X - antenna.X;
            double dY = satellite.Y - antenna.Y;
            double dZ = satellite.Z - antenna.Z;

            return antenna.ToGeodesic().ApparentPosition(dX, dY, dZ);
        }

        public static ApparentPosition ApparentPosition(this GeodesicCoordinates geodesic, double dX, double dY, double dZ)
        {
            double sinB = Sin(geodesic.Latitude);
            double sinL = Sin(geodesic.Longitude);
            double cosB = Cos(geodesic.Latitude);
            double cosL = Cos(geodesic.Longitude);

            double u = -sinB*cosL*dX - sinB*sinL*dY + cosB*dZ;
            double v = -sinL*dX + cosL*dY;
            double w = cosB*cosL*dX + cosB*sinL*dY + sinB*dZ;

            double az = (Atan2(v, u) + 2*PI) % (2*PI);
            double el = PI/2 - Atan2(Sqrt(Pow(u, 2) + Pow(v, 2)), w);

            return new ApparentPosition(azimuth: az, elevation: el);
        }

    }
}