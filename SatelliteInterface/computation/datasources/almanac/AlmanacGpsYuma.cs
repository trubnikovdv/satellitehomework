using System;
using SatelliteInterface.computation.coordinates;
using SatelliteInterface.computation.utils;

namespace SatelliteInterface.computation.datasources.almanac
{
    using static TimeUtils;
    using static KeplerUtils;
    
    /**
     * http://www.celestrak.com/GPS/almanac/Yuma/definition.php
     */
    public class AlmanacGpsYuma : IDataSource
    {
        private readonly int mWeek;
        private readonly double mEccentricity;                // e
        private readonly double mTimeOfApplicability;         // t_oa
        private readonly double mOrbitalInclination;          // i
        private readonly double mRightAscensionRate;          // dΩ/dt
        private readonly double mSemiMajorAxis;               // a
        private readonly double mRightAscension;              // Ω_o
        private readonly double mPerigeeArgument;             // ω
        private readonly double mMeanAnomaly;                 // M_o
        private readonly double mZeroOrderClockCorrection;    // a_fo
        private readonly double mFirstOrderClockCorrection;   // a_f1

        public AlmanacGpsYuma(int week,
            double eccentricity,
            double timeOfApplicability,
            double orbitalInclination,
            double rightAscensionRate,
            double sqrtSemiMajorAxis,
            double rightAscension,
            double perigeeArgument,
            double meanAnomaly,
            double zeroOrderClockCorrection,
            double firstOrderClockCorrection)
        {
            mWeek = week;
            mEccentricity = eccentricity;
            mTimeOfApplicability = timeOfApplicability;
            mOrbitalInclination = orbitalInclination;
            mRightAscensionRate = rightAscensionRate;
            mSemiMajorAxis = Math.Pow(sqrtSemiMajorAxis, 2);
            mRightAscension = rightAscension;
            mPerigeeArgument = perigeeArgument;
            mMeanAnomaly = meanAnomaly;
            mZeroOrderClockCorrection = zeroOrderClockCorrection;
            mFirstOrderClockCorrection = firstOrderClockCorrection;
        }

        private void Print(string message)
        {
            Console.WriteLine(message);
        }

        public GeocentricCoordinates GetPositionForTime(DateTime time)
        {
            double deltaTime = CalculateDeltaTime(time.ToUniversalTime());
            Print($"Δt = {deltaTime} [c]");

            double meanAnomaly = CalculateMeanAnomaly(deltaTime);
            Print($"Средняя аномалия: {meanAnomaly.ToDegree()}");
            double eccentricAnomaly = CalculateEccentricAnomaly(meanAnomaly, mEccentricity);
            Print($"Эксцентрическая аномалия: {eccentricAnomaly.ToDegree()}");
            double trueAnomaly = CalculateTrueAnomaly(eccentricAnomaly, mEccentricity);
            Print($"Истинная аномалия: {trueAnomaly.ToDegree()}");

            double radius = CalculateRadius(eccentricAnomaly);
            Print($"Радиус: {radius.Round()} [м]");
            double latitudeArg = mPerigeeArgument + trueAnomaly;
            Print($"Аргумент широты: {latitudeArg.ToDegree()}");
            double ascendingNode = CalculateAscendingNodeLatitude(deltaTime);
            Print($"Долгота восходящего узла: {ascendingNode.ToDegree()}");

            var polarCoordinates = new PolarCoordinates(latitudeArg, radius);
            Print($"Полярные координаты спутника: {polarCoordinates}");
            var orbitalCoordinates = new OrbitalCoordinates(polarCoordinates);
            var geocentricCoordinates = new GeocentricCoordinates(orbitalCoordinates, ascendingNode, mOrbitalInclination);
            Print($"Геоцентрические координаты спутника: {geocentricCoordinates}");
            Print($"Геодезические координаты спутника: {geocentricCoordinates.ToGeodesic()}");

            return geocentricCoordinates;
        }
        
        private double CalculateDeltaTime(DateTime time)
        {
            double timeOfWeek = time.TimeOfWeek();
            double gpsTime = timeOfWeek + CountLeapSeconds(time);

            double deltaTime = gpsTime - mTimeOfApplicability;

            if (gpsTime < -SECONDS_IN_WEEK / 2.0)
                deltaTime += SECONDS_IN_WEEK;
            else if (gpsTime > SECONDS_IN_WEEK / 2.0)
                deltaTime -= SECONDS_IN_WEEK;

            return deltaTime;
        }

        private double CalculateMeanAnomaly(double deltaTime)
        {
            double meanVelocity = Math.Sqrt(EARTH_GRAVI_CONSTANT / Math.Pow(mSemiMajorAxis, 3));

            double meanAnomaly = mMeanAnomaly + deltaTime * meanVelocity;
            return meanAnomaly.Normalize();
        }

        private double CalculateRadius(double eccentricAnomaly)
        {
            double cosEccentricAnomaly = Math.Cos(eccentricAnomaly);

            return mSemiMajorAxis * (1 - mEccentricity * cosEccentricAnomaly);
        }

        private double CalculateAscendingNodeLatitude(double deltaTime)
        {
            double ascendingNode = mRightAscension 
                   + (mRightAscensionRate - EARTH_ROTATION_RATE) * deltaTime 
                   - EARTH_ROTATION_RATE * mTimeOfApplicability;

            return ascendingNode.Normalize();
        }
    }
}