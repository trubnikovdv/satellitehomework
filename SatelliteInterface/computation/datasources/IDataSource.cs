using System;
using SatelliteInterface.computation.coordinates;

namespace SatelliteInterface.computation.datasources
{
    public interface IDataSource
    {
        GeocentricCoordinates GetPositionForTime(DateTime time);
    }
}